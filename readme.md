## Rotas
As rotas serão dadas como um gráfico direcionado, onde rota é representada por uma cidade de origem (sourece), uma cidade de destino (target) e a distancia entre as duas (distance). Uma determinada rota nunca aparecerá mais de uma vez e, para uma determinada rota, a cidade inicial e final não será a mesma cidade.

O gráfico direcionado será representado como texto simples, onde as cidades são nomeadas usando letras do alfabeto. Uma rota da cidade A para a cidade B com a distância 5 é representada pela cadeia ** AB5 **. Para inserir a rota no sistema, será de acordo com o JSON:

```
{ 
  "source": "A",
  "target": "B",
  "distance":5
}
```

### Gráfico

Esse endpoint deve receber um gráfico e o mesmo deve ser salvo no banco dedados. Segue o modelo da chamada do Rest.

* Endpoint: `http://<host>:<port>/graph`
* HTTP Method: POST
* Body

```
{
  "data":[
    { 
      "source": "A", "target": "B", "distance":6
    },
    { 
      "source": "A", "target": "E", "distance":4
    },
    { 
      "source": "B", "target": "A", "distance":6
    },
    { 
      "source": "B", "target": "C", "distance":2
    },
    { 
      "source": "B", "target": "D", "distance":4
    },
    { 
      "source": "C", "target": "B", "distance":3
    },
    { 
      "source": "C", "target": "D", "distance":1
    },
    { 
      "source": "C", "target": "E", "distance":7
    },
    { 
      "source": "D", "target": "B", "distance":8
    },
    { 
      "source": "E",  "target": "B", "distance":5
    },
    { 
      "source": "E", "target": "D", "distance":7
    }
  ]
}
```

### Recuperando o gráfico

Esse endpoint deve recuperar o gráfico salvo anteriormente, pelo id do mesmo.

* Endpoint: `http://<host>:<port>/graph/<graph id>`
* HTTP Method: GET
* Response

```
{
  "id" : 1,
  "data":[
    { 
      "source": "A", "target": "B", "distance":6
    },
    { 
      "source": "A", "target": "E", "distance":4
    },
    { 
      "source": "B", "target": "A", "distance":6
    },
    { 
      "source": "B", "target": "C", "distance":2
    },
    { 
      "source": "B", "target": "D", "distance":4
    },
    { 
      "source": "C", "target": "B", "distance":3
    },
    { 
      "source": "C", "target": "D", "distance":1
    },
    { 
      "source": "C", "target": "E", "distance":7
    },
    { 
      "source": "D", "target": "B", "distance":8
    },
    { 
      "source": "E",  "target": "B", "distance":5
    },
    { 
      "source": "E", "target": "D", "distance":7
    }
  ]
}
```

### Encontrar rotas disponíveis de um determinado par de cidades no gráfico salvo

Este endpoint deve calcular todas as rotas disponíveis de qualquer par de cidades dentro de um determinado número máximo de paradas em um gráfico salvo anteriormente. Se não houver rotas disponíveis, o resultado deve ser uma lista vazia. Caso o parâmetro "maxStops" não seja fornecido, deve listar todas as rotas para o par de cidades.

Para o seguinte gráfico (AB5, BC4, CD8, DC8, DE6, AD5, CE2, EB3, AE7), as rotas possíveis de A para C com o máximo de 3 paradas, deve retornar: ["ABC", "ADC", "AEBC"]

* Endpoint: `http://<host>:<port>/routes/<graph id>/from/<town 1>/to/<town 2>?maxStops=<maximum number of stops>`
* HTTP Method: POST
* Request body: vazio
* Response:

```
{
  "routes": [
    {
      "route": "ABC",
      "stops": 2
    },
    {
      "route": "ADC",
      "stops": 2
    },
    {
      "route": "AEBC",
      "stops": 3
    }
  ]
}
```

### Encontre a distância do caminho no gráfico salvo

Esse endpoint deve receber uma lista ordenada de cidades e recuperar a distância total ao percorrer a lista de cidades na ordem em que aparecem na solicitação em um gráfico salvo anteriormente.

* Endpoint: `http://<host>:<port>/distance/<graph id>`
* HTTP Method: POST
* Request body:

```
{
  "path":["A", "B", "C", "D"]
}
```

  * Response:

```javascript
{
  "distance" : 9
}
```

### Encontre distância entre duas cidades no gráfico salvo

Esse endpoint deve encontrar o caminho mais curto entre duas cidades em um gráfico salvo anteriormente. Se as cidades inicial e final forem as mesmas, o resultado deve ser zero.

* Endpoint: `http://<host>:<port>/distance/<graph id>/from/<town 1>/to/<town 2>`
* HTTP Method: POST
* Request body: vazio
* Response payload

```
{
  "distance" : 8,
  "path" : ["A", "B", "C"]
}
```

## Testes

Gráfico:
AB5, BC4, CD8, DC8, DE6, AD5, CE2, EB3, AE7

Casos de testes:
1. Distância da rota ABC: 9
2. Distância da rota AD: 5
3. Distância da rota ADC: 13
4. Distância da rota AEBCD: 22
5. Distância da rota AED: NO SUCH ROUTE
6. Rota começando em C e terminando em C com o máximo de 3 paradas: 
	- CDC  (2 paradas)
	- CEBC (3 paradas)
7. Rota começando em A e terminando em C com o máximo de 4 paradas:
	- ABC   (2 paradas)
	- ADC   (2 paradas)
	- AEBC  (3 paradas)
	- ADEBC (4 paradas)
8. Menor rota de A para C: ABC  (distancia = 9)
9. Menor rota de B para B: B (distancia = 0)

