package com.avenuecode.test;

import static org.junit.Assert.assertEquals;

import java.util.ArrayList;
import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import com.bestroute.dto.InnerRouteDTO;
import com.bestroute.model.Graph;
import com.bestroute.model.Route;
import com.bestroute.service.RouteService;

@RunWith(SpringRunner.class)
@SpringBootTest
public class RouteTest {
	
	private RouteService service = new RouteService();
	
	@Test
	public void testeGetRouteList() {
		List<InnerRouteDTO> routes = service.getAvaiableRoutes(getGraph(), "A", "C", 4);
		assertEquals(routes.size(), 4);
	}
	
	private Graph getGraph() {
		Graph graph = new Graph();
		List<Route> routes = new ArrayList<>();
		routes.add(new Route("A","B",5));
		routes.add(new Route("B","C",4));
		routes.add(new Route("C","D",8));
		routes.add(new Route("D","C",8));
		routes.add(new Route("D","E",6));
		routes.add(new Route("A","D",5));
		routes.add(new Route("C","E",2));
		routes.add(new Route("E","B",3));
		routes.add(new Route("A","E",7));
		graph.setData(routes);
		return graph;
	}
}
