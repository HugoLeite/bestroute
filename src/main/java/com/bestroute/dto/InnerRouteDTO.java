package com.bestroute.dto;

public class InnerRouteDTO {

	public String path;
	public Integer distance;

	public InnerRouteDTO(String route, Integer stops) {
		this.path = route;
		this.distance = stops;
	}
	
	public InnerRouteDTO() {
	}

	public String getPath() {
		return path;
	}

	public void setPath(String route) {
		this.path = route;
	}

	public Integer getDistance() {
		return distance;
	}

	public void setDistance(Integer stops) {
		this.distance = stops;
	}

	
}
