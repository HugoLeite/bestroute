package com.bestroute.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.bestroute.model.Graph;

@Repository
public interface GraphRepository extends CrudRepository<Graph, Long> {

	
	
	
}
