package com.bestroute.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.bestroute.model.Route;

@Repository
public interface RouteRepository extends CrudRepository<Route, Long> {

}
