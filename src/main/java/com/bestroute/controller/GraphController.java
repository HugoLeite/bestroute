package com.bestroute.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.bestroute.model.Graph;
import com.bestroute.repository.GraphRepository;

@CrossOrigin("*")
@RestController
@RequestMapping(path = "/graph")
public class GraphController {

	@Autowired
	private GraphRepository repository;

	// 1.Save graph configuration
	@PostMapping
	public Graph createGraph(@RequestBody Graph graph) {
		repository.save(graph);
		return graph;

	}

	// 2.Retrieve graph configuration
	@GetMapping("/{id}")
	public Graph getGraph(@PathVariable Long id) throws Exception {
		Graph graph = repository.findOne(id);
		if (graph != null) {
			return graph;
		} else {
			throw new Exception("NOT FOUND");
		}
	}

}
